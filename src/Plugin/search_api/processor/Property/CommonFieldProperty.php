<?php

namespace Drupal\search_api_common_field\Plugin\search_api\processor\Property;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Processor\ConfigurablePropertyBase;

/**
 * Defines a property common to multiple datasources.
 *
 * This allows the selection of properties that have the same name on different
 * datasources.
 */
class CommonFieldProperty extends ConfigurablePropertyBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'property_name' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(FieldInterface $field, array $form, FormStateInterface $form_state) {
    $index = $field->getIndex();
    $configuration = $field->getConfiguration() + $this->defaultConfiguration();

    // Find properties common to multiple datasources.
    $common_properties = [];
    $property_labels = [];
    $datasource_ids = $index->getDatasourceIds();
    $datasources = $index->getDatasources();

    foreach ($datasources as $datasource_id => $datasource) {
      foreach ($index->getPropertyDefinitions($datasource_id) as $property_path => $property) {
        $common_properties[$property_path][] = $datasource_id;
        $property_labels[$property_path] = $property->getLabel();
      }
    }
    // Remove properties which are only on one datasource.
    $common_properties = array_filter($common_properties, fn ($datasource_ids) => count($datasource_ids) > 1);

    $options = [];
    foreach ($common_properties as $property_path => $datasource_ids) {
      $options[$property_path] = $this->t("@property-label (used in @datasources)", [
        '@property-label' => $property_labels[$property_path],
        '@datasources' => implode(', ', array_map(
          fn ($datasource_id) => $datasources[$datasource_id]->label(),
          $datasource_ids,
        )),
      ]);
    }

    $form['property_name'] = [
      '#type' => 'radios',
      '#title' => $this->t('Common field'),
      '#options' => $options,
      '#default_value' => $configuration['property_name'],
      '#required' => TRUE,
    ];

    return $form;
  }

}

