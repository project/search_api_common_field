<?php

namespace Drupal\search_api_common_field\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api_common_field\Plugin\search_api\processor\Property\CommonFieldProperty;

/**
 * Adds values for common fields to indexed items.
 *
 * This works similarly to the AggregatedFields processor in Search API.
 *
 * @SearchApiProcessor(
 *   id = "common_field",
 *   label = @Translation("Common fields"),
 *   description = @Translation("Merges properties with the same name on different datasources."),
 *   stages = {
 *     "add_properties" = 20,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class CommonField extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Common field'),
        'description' => $this->t('A field common to multiple datasources.'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['common_field'] = new CommonFieldProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $fields = $item->getFields();
    $common_fields = $this->getFieldsHelper()
      ->filterForPropertyPath($fields, NULL, $this->pluginDefinition['id']);

    $datasource_ids = $this->index->getDatasourceIds();

    foreach ($common_fields as $field_id => $common_field) {
      $source_property_id = $common_field->getConfiguration()['property_name'];

      // Create a $required_properties array to pass to extractItemValues(). It
      // doesn't matter that the return value ID is the same for all
      // datasources, as only one datasource will have a value.
      $required_properties = array_fill_keys(
        $datasource_ids,
        [
          $source_property_id => $source_property_id,
        ],
      );

      $property_values = $this->getFieldsHelper()
        ->extractItemValues([$item], $required_properties)[0];

      $datasource_values = reset($property_values);

      foreach ($datasource_values as $value) {
        $common_field->addValue($value);
      }
    }
  }

}
