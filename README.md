# Search API Common Fields

This module allows defining fields on Search API index which merge matching
properties on multiple datasources.

For example, if two datasources on an index have an identically-named date
field, this module allows a single field which provides a value for items from
either datasource. This means that items from both datasources can use the same
field for indexing, filtering, sorting, and display. This simplifies the
building of components such as facets and views.

## Requirements

This module requires the following modules:

- [Search API](https://www.drupal.org/project/search_api)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal
Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. At the Fields page for your search index, click 'Add fields'
2. Select 'Common field'
3. Select which common property to use.

